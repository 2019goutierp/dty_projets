# Space Canon

Space Canon est une simulation du canon de Newton, une expérience de pensée pour montrer l'importance de la gravité dans les forces qui régissent l'univers.

## Installation

Le module PIL est nécessaire pour faire fonctionner cette simulation

```bash
pip install PIL
```

## Utilisation

Télécharger le fichier 'Images' et le fichier 'Space_Canon.py'.
Lancer le fichier Space_Canon.py.