# Project GOLDMAN

Project Goldman est designé pour créer de nouveaux blindtests à partir de sa base de données ou à partir de vos musiques.

## Installation

Quelques modules sont nécessaires avant de faire fonctionner le project GOLDMAN :

```bash
pip install flask
pip install pytube 
pip install moviepy
pip install wtforms
pip install rfc3987
```

## Utilisation

Télécharger le dossier dans son intégralité.

Ouvrir le projet et lancer le programme app.py. Le site s'ouvrira à partir de l'adresse : localhost:5000.
À partir de là, à vous de jouer !

**Attention** : au moment de lancer la partie, un temps de chargement un peu long peut avoir lieu. Veuillez attendre que la partie se lance automatiquement.
